﻿using EmAspCore.Util;
using System.Data.SqlClient;

namespace EmAspCore.Services
{
    public abstract class BaseService
    {
        // コネクション
        protected SqlConnection con;
        protected SqlTransaction tran;

        protected BaseService(bool autoCommit)
        {
            this.con = DBUtil.getConnection();
            if (!autoCommit)
            {
                tran = con.BeginTransaction();
            }
        }
    }
}
